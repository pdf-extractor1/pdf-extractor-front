import React from "react";
import "./SelectionRectangle.scss"
import ContentEditable, { ContentEditableEvent } from "react-contenteditable";

interface SelectionRectangleProps {
  name?: string
  onNameChange?: (name: string) => void
  disabled?: boolean
  x1: number
  x2: number
  y1: number
  y2: number
}

export default class SelectionRectangle extends React.Component<SelectionRectangleProps, any> {
  nameRef = React.createRef<HTMLInputElement>()

  constructor(props: Readonly<SelectionRectangleProps> | SelectionRectangleProps) {
    super(props);
    this.state = {
      name: props.name
    }
  }

  focusInput() {
    let input = this.nameRef.current;
    if (input)
      input.focus()
  }

  private onNameChange() {
    const div = this.nameRef.current;
    if(!div)
      return

    const newName = div.textContent || ''
    this.setState({name: newName})
    if (this.props.onNameChange)
      this.props.onNameChange(newName)

    div.blur()
  }

  private handleKeyDown(event: React.KeyboardEvent<HTMLDivElement>) {
    if(event.key === 'Enter') {
      event.preventDefault()
      this.onNameChange()
    }
  }

  private handleBlur(event: React.FocusEvent<HTMLDivElement>) {
    this.onNameChange()
  }

  render() {
    const x3 = Math.min(this.props.x1, this.props.x2);
    const x4 = Math.max(this.props.x1, this.props.x2);
    const y3 = Math.min(this.props.y1, this.props.y2);
    const y4 = Math.max(this.props.y1, this.props.y2);

    const style = {
      left: `${x3}px`,
      top: `${y3}px`,
      width: `${x4 - x3}px`,
      height: `${y4 - y3}px`
    }

    return <div style={style}>
      <ContentEditable
          innerRef={this.nameRef} html={this.state.name || ''}
          onKeyDown={event => this.handleKeyDown(event)}
          onBlur={event => this.handleBlur(event)}
          onChange={event => this.setState({name: event.target.value})}/>
      <div className="rectangle"/>
    </div>
  }
}
