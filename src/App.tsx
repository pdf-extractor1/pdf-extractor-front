import React from 'react';
import './App.scss';
import SelectionCanvas from "./components/SelectionCanvas/SelectionCanvas";

function App() {
  return (
    <div className="App">
      <SelectionCanvas/>
    </div>
  );
}

export default App;
