import React, { RefObject } from "react";
import { Field } from "../../model/Field";
import SelectionRectangle from "../SelectionRectangle/SelectionRectangle";
import "./SelectionCanvas.scss"

interface FieldContainer {
  ref: RefObject<SelectionRectangle>
  data: Field
}

interface State {
  currentField?: FieldContainer;
  fields: FieldContainer[];
}

export default class SelectionCanvas extends React.Component<any, State> {
  canvasRef = React.createRef<HTMLDivElement>()

  constructor(props: Readonly<any> | any) {
    super(props);
    this.state = {
      fields: []
    }
  }

  private startRectangle(event: React.MouseEvent<HTMLDivElement>) {
    if (event.target !== this.canvasRef.current || document.activeElement !== document.body) {
      return;
    }

    this.setState({
      currentField: {
        ref: React.createRef(),
        data: {
          rectangle: {
            x1: event.clientX,
            y1: event.clientY,
            x2: event.clientX,
            y2: event.clientY
          }
        }
      }
    })
  }

  private updateRectangle(e: React.MouseEvent<HTMLDivElement>) {
    let field = this.state?.currentField;
    if (field) {
      field.data.rectangle = {
        ...field.data.rectangle,
        x2: e.clientX,
        y2: e.clientY
      }

      this.setState({currentField: field});
    }
  }

  private finishRectangle(e: React.MouseEvent<HTMLDivElement>) {
    let currentField = this.state.currentField;
    if (currentField) {
      let rec = currentField.data.rectangle;
      if(Math.abs(rec.x1 - rec.x2) + Math.abs(rec.y1 - rec.y2) < 10)
        return;

      this.setState({
        currentField: undefined,
        fields: [
          ...this.state.fields,
          currentField
        ]
      }, () => {
        let selection = currentField?.ref.current;
        if (selection)
          selection.focusInput()
      })
    }
  }

  render() {
    let cf = this.state?.currentField;
    return <div id="canvas" ref={this.canvasRef}
                onMouseDown={(e) => this.startRectangle(e)}
                onMouseMove={(e) => this.updateRectangle(e)}
                onMouseUp={(e => this.finishRectangle(e))}>
      {cf ? <SelectionRectangle ref={cf.ref} {...cf.data.rectangle}/> : null}
      {this.state.fields.map((f, i) =>
          <SelectionRectangle name={f.data.name} {...f.data.rectangle}
                              onNameChange={(name => {
                                console.log(name)
                              })}
                              key={i} ref={f.ref}/>)}
    </div>;
  }
}
