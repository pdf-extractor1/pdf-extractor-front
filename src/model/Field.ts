import { Rectangle } from "./Rectangle";

export interface Field {
  name?: string;
  rectangle: Rectangle;
}
